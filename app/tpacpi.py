FAN_FILE = "/proc/acpi/ibm/fan"
VALID_LEVELS = [
    "auto",
    "disengaged",
    "full-speed",
    "0",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
]


def send_command(command):
    with open(FAN_FILE, "w") as f:
        f.write(command)
        f.write("\n")


def read_prop(key):
    with open(FAN_FILE, "r") as f:
        for line in f:
            if key in line:
                try:
                    return line.split()[1]
                except IndexError:
                    continue

    return "unknown"
