from flask import Blueprint, render_template, request, abort

from .tpacpi import read_prop, send_command, VALID_LEVELS

bp = Blueprint("views", __name__)


@bp.route("/", methods=["GET", "POST"])
def main_page():
    if request.method == "POST":
        level = request.form.get("level")
        if level not in VALID_LEVELS:
            abort(400)
        send_command(f"level {level}")

    return render_template(
        "index.html",
        levels=VALID_LEVELS,
        current_level=read_prop("level"),
        current_speed=read_prop("speed"),
    )


@bp.route("/get-current-speed")
def get_current_speed():
    return read_prop("speed")
