# tp-fanboy

A simple Flask application that allows you to control a ThinkPad laptop's fan
speed using a web interface. This application works by utilizing ThinkPad ACPI
fan control interface (`/proc/acpi/ibm/fan`). It is tested working on my
personal ThinkPad T470.

This is an unsubmitted final project for my System Programming (CSCM603127)
course.

**Disclaimer:** Please be aware that this software is not intended for
production purpose. It has not been tested nor designed to be a fully-working
product. Use it at your own risk!

![image](https://user-images.githubusercontent.com/30001379/212955303-04800107-efc4-42d9-ab38-5240706cf978.png)


## Testing the application

The following steps assume that you are running as `root`.

1.  Make sure your ThinkPad laptop has the ThinkPad ACPI fan control interface
2.  Clone or download the repository
3.  Run `pip install flask` to install the Flask package (you can use a virtual
    environment to prevent extra clutter)
4.  Run the application with `python wsgi.py`

## License

This project is licensed under the MIT License.
